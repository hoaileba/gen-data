from flask import Flask, render_template, redirect, url_for, request, send_file, jsonify
from flask_socketio import SocketIO, emit
import requests
import logging
import sys
import argparse
import os
import json
import random
import glob
import re
import datetime
import pandas as pd
from tqdm import tqdm
import zipfile
import os 
import requests
import datetime
from os.path import exists
import random
import re


logging.basicConfig(level=logging.INFO)
HOST = "103.141.141.13"
app = Flask(__name__)
app.config["SECRET_KEY"] = os.getenv('SECRET_KEY')
socketio = SocketIO(app)
stories_list = []

parser = argparse.ArgumentParser()
#parser.add_argument("--ps", type=int, help="Port of Server running", default = 6999)

args = parser.parse_args()
stories_generator = None

messages_dict = dict()
SESSION_TO_DATA = {}

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), 
                       os.path.relpath(os.path.join(root, file), 
                                       os.path.join(path, '..')))

def backtrack(slots_list, slots_values, i, res, id=0):
    '''
        slots_list: list of slots
        slots_values: dictionary which each key contain possible values
        i: iterator
        res: result of the previous loop
    '''
    global messages_dict
    messages_list = messages_dict.get(id)
    # print(intent_text)
    slot_name = slots_list[i]
    values = slots_values[slot_name]
    # print(intents)
    for value in values:
        current_result = (res + '.')[:-1]
        current_result = current_result.replace('{' + slot_name + '}', value)
        if i == len(slots_list) - 1:
            # print(current_result)
            messages_list.append(current_result)
        else:
            backtrack(slots_list, slots_values, i+1, current_result, id)

def shuffle_method(slots_list, slots_values, amount, message):
    '''
        slots_list: list of slots
        slots_values: dictionary which each key contain possible values
        amount: amount of data you want to generate
        message: message with slot
    '''
    result = []

    slots_shuffle = {}

    for slot_name in slots_list:
        slots_shuffle[slot_name] = []
        for i in range(amount):
            slots_shuffle[slot_name].append(slots_values[slot_name][i % len(slots_values[slot_name])])
        random.shuffle(slots_shuffle[slot_name])

    for i in range(amount):
        temp_message = '' + message
        for slot_name in slots_list:
            value = slots_shuffle[slot_name][i]
            temp_message = temp_message.replace('{' + slot_name + '}', value)
        result.append(temp_message)

    return result

def process_slots(id,template,slots,number):
    messages_dict[str(id)] = []
    # slots = json_data.get('slots')
    message = template
    amount = int(number)

    # define variable
    slots_values = {}
    slots_list = []
    backtrack_total_amount = 1

    # extract data from request
    for key in slots:
        slots_list.append(key)
        slot_arr = slots.get(key).split(',')
        slot_amount = len(slot_arr)
        backtrack_total_amount *= slot_amount
        slots_values.update({key: slot_arr})

    # route generator method
    if backtrack_total_amount > 1e5:
        result = shuffle_method(slots_list, slots_values, amount, message)
        messages_dict[str(id)] = result
    else:
        backtrack(slots_list, slots_values, 0, message, id=str(id))
        random.shuffle(messages_dict[str(id)])
        if amount < len(messages_dict[str(id)]):
            messages_dict[str(id)] = messages_dict[str(id)][:amount]
    return messages_dict


def check_data(data_sheet):
    col_intent = data_sheet.get("Data Gen Format").get("Intent")
    col_template = data_sheet.get("Data Gen Format").get("Mẫu câu gen")
    col_slot = data_sheet.get("Data Gen Format").get("tên slot")
    col_value_slot = data_sheet.get("Data Gen Format").get("Giá trị")
    col_num = data_sheet.get("Data Gen Format").get("Số Lượng")
    result = {}
    index = {}
    result["status"] = "OK"
    result["message"] = []

    for id,intent in  enumerate(col_intent):
        str_col_value_slot = str(col_value_slot[id])
        str_col_slot = str(col_slot[id])
        if str_col_slot in ["nan","Nan","NAN","NaN"] and str_col_value_slot in ["nan","Nan","NAN","NaN"]:
            continue
        if isinstance(intent,str):
            # intent = intent.strip()
            data_template = {}
            if intent is not None and intent != "":
                # print(id,intent)
                intent = intent.lower()
                for id_temp,template in enumerate(col_template[id:]):
                    id_temp+= id
                    cur_intent = col_intent[id_temp]
                    if isinstance(cur_intent,str) and cur_intent != intent:
                        break
                        
                    if isinstance(template,str) :
                        template = template.strip().lower()
                        template = " ".join(template.split())
                        template = template.strip()
                        data_template[template] = {}
                        number = col_num[id_temp]
                        
                        if (str(number) in ["nan","Nan","NAN","NaN"]):
                            number = col_num[min(id_temp-1,0)]
                        
                        data_template.update({"number":number})
                        slot = re.findall('\{(.*?)\}', template)
                        number_slot = len(re.findall('\{(.*?)\}', template))
                        template_slot = {}
                        for s in slot:
                            s = s.lower()
                            s = s.strip()
                            template_slot.update({s:""})
                            
                        template_in_slot = ""
                        for i in range(number_slot):
                            norm_slot = str(col_slot[id_temp+i]).strip().lower()
                            template_in_slot += norm_slot+" "
                            norm_value_slot = str(col_value_slot[id_temp+i]).strip().lower()
                            norm_slot = re.sub(r'[}{]',"", norm_slot)
                            template_slot.update({norm_slot:norm_value_slot})
                            
                        if template != template_in_slot.strip():
                            result["status"] = "ERROR"
                            message = "ERROR : " + intent +" |--- template :    "+template+" |--- from_slot: " +template_in_slot
                            result["message"].append(message)

    return result


def extract_slots(session_id,data_sheet,path_save):
    session_id = str(session_id)
    result_status = check_data(data_sheet)
    if result_status.get("status") == "ERROR":
        return result_status
    col_intent = data_sheet.get("Data Gen Format").get("Intent")
    col_template = data_sheet.get("Data Gen Format").get("Mẫu câu gen")
    col_slot = data_sheet.get("Data Gen Format").get("tên slot")
    col_value_slot = data_sheet.get("Data Gen Format").get("Giá trị")
    col_num = data_sheet.get("Data Gen Format").get("Số Lượng")
    result = {}
    index = {}
            
    for id,intent in  enumerate(col_intent):
        str_col_value_slot = str(col_value_slot[id])
        str_col_slot = str(col_slot[id])
        if str_col_slot in ["nan","Nan","NAN","NaN"] and str_col_value_slot in ["nan","Nan","NAN","NaN"]:
            continue
        if isinstance(intent,str):
            result[intent] = []
            data_template = {}
            
            if intent is not None and intent != "":
                path_save_intent = os.path.join(path_save,intent+".md")

                f = open(path_save_intent,"w")
                f.write(f"## intent:{intent}\n")
                intent = intent.lower()
                for id_temp,template in enumerate(col_template[id:]):
                    id_temp+= id
                    cur_intent = col_intent[id_temp]
                    if isinstance(cur_intent,str) and cur_intent != intent:
                        break
                        
                    if isinstance(template,str) :
                        template = template.strip().lower()
                        data_template[template] = {}
                        number = col_num[id_temp]
                        
                        if (str(number) == "nan"):
                            number = col_num[min(id_temp-1,0)]
                        
                        data_template[template].update({"number":number})
                        slot = re.findall('\{(.*?)\}', template)
                        number_slot = len(re.findall('\{(.*?)\}', template))
                        template_slot = {}
                        for s in slot:
                            s = s.lower()
                            s = s.strip()
                            template_slot.update({s:""})
                        
                        for i in range(number_slot):
                            norm_slot = str(col_slot[id_temp+i]).strip().lower()
                            norm_value_slot = str(col_value_slot[id_temp+i]).strip().lower()
                            norm_slot = re.sub(r'[}{]',"", norm_slot)
                            template_slot.update({norm_slot:norm_value_slot})
                            
                        data_template[template].update({"slot":template_slot})
                        res = process_slots(session_id,template,template_slot,number)
                        f.write(f"<!-- {template} -->\n")
                        
                        for text in res.get(session_id):
                            text = " ".join(text.split())
                            text = "- "+text.strip()+"\n"
                            f.write(text)

            if intent == "intent_no_way_improve":
                print(data_template)
            result[intent].append(data_template)
            f.close()
    
    return result_status


@app.route('/',methods=["GET", "POST"])
def index():
    global SESSION_TO_DATA
    data = {}
    if request.method == "POST":
        # data = {}
        session_id = str(request.form.get('session_id'))
        if SESSION_TO_DATA.get(session_id) is None:
            SESSION_TO_DATA[session_id] = {}
        SESSION_TO_DATA[session_id]["host"] = request.form.get("host")
        SESSION_TO_DATA[session_id]["port"] = request.form.get("port")
        data = SESSION_TO_DATA.get(session_id)
        if data is None:
            data = {}

        SESSION_TO_DATA[session_id]["session_id"] = session_id
        data = SESSION_TO_DATA.get(session_id)
        
        file = request.files['fileup']
        path = request.form.get("path").strip()
        if path == "" or path is None:
            data["status"] = "ERROR"
            data["list_error"] = ["Not found path save"]
            return render_template('index.html',data = data)
        if file:
            data_sheet = pd.read_excel(file, sheet_name=None, engine='openpyxl')
            result = extract_slots(session_id,data_sheet,path)
            if result.get("status") == "ERROR":
                data["status"] = "ERROR"
                data["list_error"] =  result.get("message") 
                return render_template('index.html',data = data)
            else :
                data["status"] = "OK"
                data["list_error"] =  result.get("message") 
                data["message"] = "Done"
                filename_path = os.path.join(path,"nlu.zip")
                return render_template('index.html',data = data)
    return render_template('index.html',data = {})

@app.route('/download')
def downloadFile():
    filename = request.args.get('filename')
    return send_file(filename, as_attachment=True)

if __name__ == '__main__':
    socketio.run(app, port=7024, host = HOST ,debug = True)


