#!/bin/bash
bin=/home/ubuntu/miniconda3/envs/flask/bin

nohup $bin/python gen_data_v2.py > gen_data.log &
pid_action=$(echo $!)
echo $pid_action > pid.pid
